﻿using System.Collections.Generic;
using System.Web.Http;
using Todos.Domain;
using Todos.DTO;

namespace Todos.WebApi.Controllers
{
    public class TodosController : ApiController
    {
        TodoService todoService = new TodoService();

        public IEnumerable<Todo> Get()
        {
            try
            {
                return todoService.GetAll();
            }
            catch (System.Exception ex)
            {
                var list = new List<Todo>();
                list.Add(new Todo() { title = ex.Message });
                return list;
            }
            
        }
    }
}
