﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todos.DTO;
using Todos.Repository;

namespace Todos.Domain
{
    public class TodoService
    {
        public IEnumerable<Todo> GetAll()
        {
            TodosDbContext context = new TodosDbContext();
            List<Todo> todos = context.TodoDbSet.ToList();
            
            return todos;
        }
    }
}
