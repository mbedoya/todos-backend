﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos.DTO
{
    public class Todo
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string title { get; set; }
        public string body { get; set; }
        public DateTime dateCreated { get; set; }
    }
}
