namespace Todos.Repository.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Todos.DTO;

    internal sealed class Configuration : DbMigrationsConfiguration<Todos.Repository.TodosDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Todos.Repository.TodosDbContext context)
        {
            /*
            context.TodoDbSet.AddOrUpdate(
              new Todo { title = "My first todo", dateCreated = new DateTime() },
              new Todo { title = "Remember to do the laundry", dateCreated = new DateTime().AddDays(-2) }
            );
            */
        }
    }
}
