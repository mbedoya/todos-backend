﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todos.DTO;

namespace Todos.Repository
{
    public class TodosDbContext : DbContext
    {
        public virtual DbSet<Todo> TodoDbSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<TodosDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
